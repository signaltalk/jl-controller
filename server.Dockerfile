FROM ruby:2.5.0-slim-stretch as builder

WORKDIR /usr/src/jl-controller

ENV TZ Asia/Tokyo
ENV LANG ja_JP.UTF-8

RUN \
  apt-get update \
  && apt-get install -y \
    apt-transport-https \
    curl \
    gnupg2 \
    --no-install-recommends \
  && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list \
  && curl -sL https://deb.nodesource.com/setup_6.x | bash - \
  && apt-get install -y \
    gcc \
    g++ \
    git \
    make \
    yarn \
    nodejs \
    --no-install-recommends \
  && apt-get clean

COPY \
  server/Gemfile \
  server/Gemfile.lock \
  server/package.json \
  server/yarn.lock \
  server/entrypoint.sh \
  ./

RUN \
  bundle install \
  && yarn install \
  && mkdir ../node \
  && mv node_modules ../node
