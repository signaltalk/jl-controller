Rails.application.routes.draw do
  get 'publish', to: 'main#publish'
  get '(*path)', to: 'main#index'
end
