require 'yaml_erb'

namespace = "#{Rails.application.class.parent_name}:#{Rails.env}"
config = YAML.load_erb(Rails.root.join('config', 'redis.yml'))[Rails.env]
Redis.current = ConnectionPool.new(config['connection_pool']) do
  Redis::Namespace.new namespace, redis: Redis.new(config['connection'])
end
Redis::Objects.redis = Redis.current
