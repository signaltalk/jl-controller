const { environment } = require('@rails/webpacker')

environment.loaders.append('coffee', {
    test: /\.coffee(\.erb)?$/,
    use: [
        'babel-loader',
        'coffeescript-loader'
    ]
})

module.exports = environment
