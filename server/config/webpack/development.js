process.env.NODE_ENV = process.env.NODE_ENV || 'development'

const environment = require('./environment')

config = environment.toWebpackConfig()
config.output.filename = '[name]-[hash].js'
config.devServer.watchContentBase = false

module.exports = config
