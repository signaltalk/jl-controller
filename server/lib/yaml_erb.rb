require 'yaml'

#
module YAML
  def self.load_erb(path)
    require 'erb'
    erb = IO.read(path)
    yaml = ERB.new(erb).result
    safe_load(yaml, [], [], true)
  end
end
