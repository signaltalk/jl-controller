import React, { Component, Fragment } from 'react'
import { Button } from 'react-bootstrap'

export default class FastButton extends Component
  constructor: (props, context) ->
    super props, context
    @state = {
      moving: false
    }
  render: ->
    self = @
    props = {
      @props...
      onClick: undefined
      children: undefined
      onTouchStart: (event) -> self.on_touch_start(event)
      onTouchMove: (event) -> self.on_touch_move(event)
      onTouchEnd: (event) -> self.on_touch_end(event)
    }
    <Button {props...}>{@props.children}</Button>
  on_touch_start: (event) -> @setState moving: false
  on_touch_move: (event) -> @setState moving: true
  on_touch_end: (event) -> @props.onClick?(event) unless @state.moving


