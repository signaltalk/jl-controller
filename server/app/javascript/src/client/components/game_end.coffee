import React, { Component, Fragment } from 'react'
import { Navbar, Badge } from 'react-bootstrap'
import Button from './fast_button'
import connect from '../common/container'
import { control_channel as ch_actions } from '../channels/actions'
import { push } from 'react-router-redux'
import Config from '../config'

format_diff = (n) ->
  if n >= 0
    "+#{n}"
  else
    "#{n}"

export default GameEnd = connect(
  ({ control_props, game_end, start }) ->
    { control_props, game_end, start }
  (dispatch) ->
    command: (payload) -> dispatch ch_actions.command(payload)
    push: (path) -> dispatch push path
) class GameEnd extends Component
  componentWillMount: -> @update_state @props
  componentWillReceiveProps: (nextProps) -> @update_state nextProps
  update_state: (props) ->
    switch
      when not props.game_end?
        @props.push '/main'
      when props.start?
        @props.push '/start'
  submit: (ok=true) ->
    if Config.DEBUG_ON_SINGLE
      @props.command { type: 'GAME_END_OK', id: i, ok } for i in [1..4]
    else
      @props.command { type: 'GAME_END_OK', id: @props.control_props.own_id, ok }
  back: ->
    @props.command { type: 'GAME_END_CANCEL' }
  render: ->
    if not @props.game_end?
      <p>loading...</p>
    else
      g = @props.game_end
      console.log 'game_end', g.toJS()
      ok = g.get('ok')
      <div className="body game-end">
        <Navbar>
          <Navbar.Header>
            <h1>ゲーム終了</h1>
          </Navbar.Header>
        </Navbar>
        <div className="table">
          <div className="table-col">

            <div className="result-frame">
              <div className="rank">{g.getIn(['rank', 3])}位</div>
              <div className="score">{g.getIn(['score', 3])}</div>
              <div className="chip">○×{format_diff g.getIn(['chip', 3])}</div>
              <div className="pt">{format_diff g.pt_total(3)}pt</div>
            </div>

          </div>
          <div className="table-col">

            <div className="result-frame">
              <div className="rank">{g.getIn(['rank', 2])}位</div>
              <div className="score">{g.getIn(['score', 2])}</div>
              <div className="chip">○×{format_diff g.getIn(['chip', 2])}</div>
              <div className="pt">{format_diff g.pt_total(2)}pt</div>
            </div>

            <div className="result-frame">
              <div className="rank">{g.getIn(['rank', 0])}位</div>
              <div className="score">{g.getIn(['score', 0])}</div>
              <div className="chip">○×{format_diff g.getIn(['chip', 0])}</div>
              <div className="pt">{format_diff g.pt_total(0)}pt</div>
            </div>

          </div>

          <div className="table-col">

            <div className="result-frame">
              <div className="rank">{g.getIn(['rank', 1])}位</div>
              <div className="score">{g.getIn(['score', 1])}</div>
              <div className="chip">○×{format_diff g.getIn(['chip', 1])}</div>
              <div className="pt">{format_diff g.pt_total(1)}pt</div>
            </div>

          </div>

        </div>
        <div className="footer submit-buttons">
          {
            if g.get('should_cancel')
              <Button onClick={=> @back() }>戻る</Button>
            else
              <div className="btn dummy">&nbsp;</div>
          }
          {
            if ok?
              <div>
                <Badge>{ ok.filter((v) -> v).size }</Badge>
                &nbsp;
                {
                  if ok.has(0)
                    attrs =
                      if ok.get(0)
                        { bsStyle: 'success', onClick: => @submit(false) }
                      else
                        { bsStyle: 'primary', onClick: => @submit() }
                    <Button bsSize="large" {attrs...}>OK</Button>
                }
              </div>
          }
        </div>
      </div>
