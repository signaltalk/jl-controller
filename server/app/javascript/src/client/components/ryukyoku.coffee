import React, { Component, Fragment } from 'react'
import connect from '../common/container'
import { Navbar, Badge } from 'react-bootstrap'
import Button from './fast_button'
import { push } from 'react-router-redux'
import { control_channel as ch_actions } from '../channels/actions'
import Config from '../config'

export default Ryukyoku = connect(
  ({ ryukyoku, control_props, game, result }) ->
    {
      selected: ryukyoku?.getIn(['selected', "#{control_props.own_id}"])
      control_props
      game
      ryukyoku
      result
    }
  (dispatch) ->
    push: (path) -> dispatch push path
    command: (payload) -> dispatch ch_actions.command(payload)
) class Ryukyoku extends Component
  componentWillMount: -> @update_state @props
  componentWillReceiveProps: (nextProps) -> @update_state nextProps
  update_state: (props) ->
    switch
      when props.result?
        @props.push '/result'
      when not props.ryukyoku?
        @props.push '/'
  ryukyoku: (selected) ->
    if Config.DEBUG_ON_SINGLE
      @props.command { type: 'RYUKYOKU_SELECTED', id: i, selected } for i in [1..4]
    else
      @props.command type: 'RYUKYOKU_SELECTED', id: @props.control_props.own_id, selected: selected
  ryukyoku_cancel: ->
    @props.command type: 'RYUKYOKU_CANCEL'
  render: ->
    if not @props.game?
      <p>loading...</p>
    else
      <div className="body">
        <Navbar>
          <Navbar.Header>
            <h1>{@props.game.kyoku_name}{@props.game.honba}本場 流局</h1>
          </Navbar.Header>
        </Navbar>
        <div className="ryukyoku-dialog">
          <div className="tenpai">
            {
              if @props.selected is true
                <Button bsStyle="success" active>テンパイ</Button>
              else
                <Button bsStyle="default" onClick={ => @ryukyoku('tenpai') }>テンパイ</Button>
            }
            {
              if @props.selected is false
                <Button bsStyle="success" active>ノーテン</Button>
              else
                <Button bsStyle="default" onClick={ => @ryukyoku('noten') }>ノーテン</Button>
            }
          </div>
          <div className="other">
            <Button onClick={ => @ryukyoku('kyushu') }>九種九牌</Button>
            <Button onClick={ => @ryukyoku('sufu') }>四風連打</Button>
            <Button onClick={ => @ryukyoku('qua-reach') }>四人リーチ</Button>
            <Button onClick={ => @ryukyoku('qua-kan') }>四槓流れ</Button>
            <Button onClick={ => @ryukyoku('tri-ron') }>三家和</Button>
          </div>
        </div>
        <div className="footer submit-buttons">
          <Button onClick={=> @ryukyoku_cancel() }>戻る</Button>
          {
            if @props.ryukyoku?.get('selected')?.size > 0
              <div>
                <Badge>{ @props.ryukyoku?.get('selected')?.size }</Badge>
              </div>
          }
        </div>
      </div>


