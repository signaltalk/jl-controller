import React, { Component, Fragment } from 'react'
import connect from '../common/container'
import { Navbar, Badge } from 'react-bootstrap'
import Button from './fast_button'
import { push } from 'react-router-redux'
import Immutable from 'immutable'
import { control_channel as channel_actions } from '../channels/actions'
import Config from '../config'

format_diff = (score) ->
  switch
    when score < 0
      "#{score}"
    when score > 0
      "+#{score}"
    else
      ''

export default Result = connect(
  ({ control_props, game, result, ryukyoku, game_end }) ->
    { control_props, game, result, ryukyoku, game_end }
  (dispatch) ->
    push: (path) -> dispatch push path
    command: (args) -> dispatch channel_actions.command(args)
    discard: ->
      dispatch type: 'RESULT_DISCARD'
) class Result extends Component
  componentWillMount: -> @update_state @props
  componentWillReceiveProps: (nextProps) -> @update_state nextProps
  update_state: (props) ->
    switch
      when props.game_end?
        @props.push '/end'
      when props.ryukyoku?
        @props.push '/ryukyoku'
      when not props.result?
        if props.agari?.winner is 0
          @props.push '/agari'
        else
          @props.push '/'
  title: ->
    switch @props.result.get('context')
      when 'ryukyoku'
        switch @props.result.get('subject')
          when 'kyushu' then '九種九牌'
          when 'sufu' then '四風連打'
          when 'qua-reach' then '四人リーチ'
          when 'qua-kan' then '四槓流れ'
          when 'tri-ron' then '三家和'
          else '流局'
      when 'agari'
        "#{@props.game.seat_name(@props.result.getIn(['subject', 'winner']))}家
         #{
          if @props.result.getIn(['subject', 'type']) is 'TSUMO'
            'ツモあがり'
          else
            'ロンあがり'
         }"
      when 'chonbo'
        "#{@props.game.seat_name(@props.result.getIn(['subject', 'target']))}家
         チョンボ
        "

  render: ->
    if not @props.result?
      <p>loading...</p>
    else
      game = @props.result.get('game')
      diff = @props.result.get('diff')
      ok = @props.result.get('ok')
      <div className="body score-result">
        <Navbar>
          <Navbar.Header>
            <h1>{game.kyoku_name}{game.honba}本場 {@title()}</h1>
          </Navbar.Header>
        </Navbar>
        <div className="table">
          <div className="table-row">
            <div className="table-front">
              { @score(2) }
            </div>
          </div>
          <div className="table-row">
            <div className="table-left">
              { @score(3) }
            </div>
            <div className="table-center">
              <div className="info-frame">
                <div className="kyoutaku">
                  <span className="reach-image">&nbsp;</span>
                  ×{
                    { current_kyoutaku } = game
                    if @props.result.get('context') is 'agari'
                      reached_count = game.reached.filter((v) -> v).size
                      current_kyoutaku + reached_count
                    else
                      current_kyoutaku
                  }
                  { format_diff diff.get('kyoutaku') }
                </div>
              </div>
            </div>
            <div className="table-right">
              { @score(1) }
            </div>
          </div>
          <div className="table-row">
            <div className="table-me">
              { @score(0) }
            </div>
          </div>
        </div>
        <div className="footer submit-buttons">
          <Button onClick={=> @back() }>戻る</Button>
          {
            if ok?
              <div>
                <Badge>{ ok.filter((v) -> v).size }</Badge>
                &nbsp;
                {
                  if ok.has(0) or Config.DEBUG_ON_SINGLE
                    attrs = 
                      if ok.get(0)
                        { bsStyle: 'success', onClick: => @submit(false) }
                      else
                        { bsStyle: 'primary', onClick: => @submit() }
                    <Button bsSize="large" {attrs...}>OK</Button>
                }
              </div>
          }
        </div>
      </div>
  submit: (ok=true) ->
    if Config.DEBUG_ON_SINGLE
      @props.command { type: 'RESULT_OK', id: i, ok } for i in [1..4]
    else
      @props.command { type: 'RESULT_OK', id: @props.control_props.own_id, ok }

  score: (i) ->
    game = @props.result.get('game')
    diff = @props.result.get('diff')
    <div className="score">
      <div className="score-row">
        <div className="score-score">{game.getIn(['score', i])}</div>
        <div className="score-diff">{format_diff diff.getIn(['score', i])}</div>
      </div>
      <div className="score-row">
        <div className="score-chip">○×{game.getIn(['chip', i])}</div>
        <div className="score-chip-diff">{format_diff diff.getIn(['chip', i])}</div>
      </div>
    </div>

  back: ->
    context = @props.result.get('context')
    switch context
      when 'ryukyoku'
        @props.command type: 'RYUKYOKU_SELECTED', id: @props.control_props.own_id, selected: null
      when 'agari'
        @props.discard()
        @props.command type: 'RESULT_CANCEL'
      when 'chonbo'
        @props.discard()
        @props.command type: 'RESULT_CANCEL'
