import React, { Component } from 'react'
import { push } from 'react-router-redux'
import { Navbar } from 'react-bootstrap'
import Button from './fast_button'
import connect from '../common/container'
import { control_props as actions } from '../control_props/actions'

export default Setup = connect(
  ({control_props}) -> {control_props}
  (dispatch) ->
    submit: (props) ->
      dispatch actions.setup(props)
      dispatch push('/')
) class Setup extends Component
  constructor: ({control_props}) ->
    super()
    @state = own_id: control_props.own_id
  set_id: (id) ->
    @setState own_id: id
  render: ->
    {own_id} = @state
    {submit} = @props

    <div className="body">
      <Navbar>
        <Navbar.Header>
          <h1>初期設定</h1>
        </Navbar.Header>
      </Navbar>
      <div className="table">
        <div className="table-row">
          <div className="table-front">
            { @toggle_button(3) }
          </div>
        </div>
        <div className="table-row">
          <div className="table-left">
            { @toggle_button(4) }
          </div>
          <div className="table-center">
            <p className="info-text">席IDを選択してください</p>
          </div>
          <div className="table-right">
            { @toggle_button(2) }
          </div>
        </div>
        <div className="table-row">
          <div className="table-me">
            { @toggle_button(1) }
          </div>
        </div>
      </div>
      <div className="footer">
        {
          if own_id?
            <Button bsStyle="primary" onClick={=> submit(@state)}>決定</Button>
          else
            <Button bsStyle="default" disabled>決定</Button>
        }
      </div>
    </div>
  toggle_button: (target) ->
    {own_id} = @state
    if own_id is target
      <Button className="main-button" bsStyle="primary" onClick={=> @set_id(null)}>{target}</Button>
    else
      <Button className="main-button" bsStyle="default" onClick={=> @set_id(target)}>{target}</Button>


