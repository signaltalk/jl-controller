import React, { Component } from 'react'
import { Badge } from 'react-bootstrap'
import Button from './fast_button'
import connect from '../common/container'
import { game as game_actions } from '../game/actions'
import { control_channel as ch_actions } from '../channels/actions'
import { push } from 'react-router-redux'
import Config from '../config'

export default Start = connect(
  ({ control_props, start }) -> { control_props, start }
  (dispatch) ->
    command: (payload) -> dispatch ch_actions.command(payload)
    push: (path) -> dispatch push path
) class Start extends Component
  constructor: ->
    super()
    @state = {roll: 0}

  componentWillMount: ->
    @update_state @props

  componentWillReceiveProps: (nextProps) ->
    @update_state nextProps

  update_state: (props) ->
    if not props.start?
      @props.push '/'
    else
      selected = props.start.get('selected')
      if selected? or props.start.get('random')?
        @update_select selected

  componentWillUnmount: ->
    @cleanup()

  seat_name: (i) ->
    s = ['東', '南', '西', '北']
    s[(4 + i) % 4]

  render: ->
    if not @props.start?
      <p>loading...</p>
    else
      { submit } = @props
      <div className="body start">
        <div className="table">
          <div className="table-row">
            <div className="table-front">
              { @toggle_button(2) }
            </div>
          </div>
          <div className="table-row">
            <div className="table-left">
              { @toggle_button(3) }
            </div>
            <div className="table-center">
              <p className="info-text">起親を選択してください</p>
            </div>
            <div className="table-right">
              { @toggle_button(1) }
            </div>
          </div>
          <div className="table-row">
            <div className="table-me">
              { @toggle_button(0) }
            </div>
          </div>
        </div>
        <div className="footer">
          {
            attrs =
              if @state.selected?
                ok = @props.start.get('ok')
                if ok?.includes(0)
                  { bsStyle: 'success', onClick: => @submit(false) }
                else
                  { bsStyle: 'primary', onClick: => @submit() }
              else
                { disabled: true }
            <div>
              {
                if ok?
                  <Badge>{ ok.size }</Badge>
              }
              <Button bsSize="large" {attrs...}>ゲーム開始</Button>
            </div>
          }
        </div>
      </div>

  toggle_button: (target) ->
    { selected, roll } = @state
    if selected?
      if selected is target
        <Button className="main-button" bsStyle="primary" onClick={=> @select(null)}>{ @seat_name(target - selected) }</Button>
      else
        <Button className="main-button" bsStyle="default" onClick={=> @select(target)}>{ @seat_name(target - selected) }</Button>
    else
      if roll is target
        <Button className="main-button" bsStyle="primary" onClick={=> @select(target)}>{ @seat_name(target - roll) }</Button>
      else
        <Button className="main-button" bsStyle="default" onClick={=> @select(target)}>{ @seat_name(target - roll) }</Button>

  select: (target) ->
    selected = target? and @props.control_props.seat_to_id(target)
    @props.command { type: 'GAME_START_SELECTED', selected }

  submit: (ok=true) ->
    if ok
      if Config.DEBUG_ON_SINGLE
        @props.command type: 'GAME_START_OK', id: i for i in [1..4]
      else
        @props.command { type: 'GAME_START_OK', id: @props.control_props.own_id }
    else
      @props.command { type: 'GAME_START_CANCEL', id: @props.control_props.own_id }


  update_select: (target = null) ->
    @cleanup()
    @start_tick 1000/10, 1500, @props.start.get('random') unless target?
    @setState {@state..., selected: target}

  start_tick: (interval, time, target) ->
    @timer_tick = setInterval(
      => @tick()
      interval
    )
    @timer_roll = setTimeout(
      =>
        @cleanup()
        @update_select(target)
      time
    )

  tick: ->
    { roll } = @state
    if roll?
      roll = ++roll % 4
      @setState {roll}

  cleanup: ->
    if @timer_roll?
      clearTimeout @timer_roll
      @timer_roll = null
    if @timer_tick?
      clearInterval @timer_tick
      @timer_tick = null
