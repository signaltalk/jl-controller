import React, { Component } from 'react'
import connect from '../common/container'
import Immutable from 'immutable'
import { push } from 'react-router-redux'
import { Modal, Tabs, Tab } from 'react-bootstrap'
import Button from './fast_button'
import { control_channel as channel_actions } from '../channels/actions'
import { game as game_actions } from '../game/actions'

export default Menu = connect(
  ({game, control_props}) -> { game, control_props }
  (dispatch) ->
    command: (args) -> dispatch channel_actions.command(args)
    push: (path) -> dispatch push path
    kyoutaku: -> dispatch game_actions.kyoutaku()
    kyoutaku_cancel: -> dispatch game_actions.kyoutaku_cancel()
) class Menu extends Component
  constructor: (props, context) ->
    super(props, context)
    @state = {
      st: do Immutable.Record(show_menu: false)
    }
  show_menu: (show) -> @setState st: @state.st.set('show_menu', show)
  restart: ->
    @props.command type: 'GAME_RESTART'
  setup: ->
    @props.push '/setup'
  game_end: ->
    @props.command type: 'GAME_END'
  chonbo: ->
    @props.command type: 'CHONBO', id: @props.control_props.own_id
  kyoutaku: ->
    @props.kyoutaku()
    @props.command type: 'GAME_KYOUTAKU', id: @props.control_props.own_id
    @show_menu false
  kyoutaku_cancel: ->
    @props.kyoutaku_cancel()
    @props.command type: 'GAME_KYOUTAKU_CANCEL', id: @props.control_props.own_id
    @show_menu false
  render: ->
    { st } = @state
    { game, className } = @props
    <div className={className}>
      <Button onClick={ => @show_menu(true) }>MENU</Button>
      <Modal show={st.show_menu} onHide={=> @show_menu(false)}>
        <Modal.Header closeButton>
          MENU
        </Modal.Header>
        <Modal.Body>
          <Tabs defaultActiveKey={1} id="menu-tabs">
            <Tab eventKey={1} title="ゲーム">
              <div className="menu-body">
                <Button onClick={=> @game_end() }>精算</Button>
                <Button onClick={=> @chonbo() }>チョンボ</Button>
                <Button onClick={=> @kyoutaku() }>供託</Button>
                {
                  if game.getIn(['kyoutakued', 0]) > 0
                    <Button onClick={=> @kyoutaku_cancel() }>供託キャンセル</Button>
                }
              </div>
            </Tab>
            <Tab eventKey={2} title="システム">
              <div className="menu-body">
                <Button onClick={=> @restart() }>ゲーム取り消し</Button>
                <Button onClick={=> @setup() }>初期設定</Button>
              </div>
            </Tab>
          </Tabs>
        </Modal.Body>
      </Modal>
    </div>
