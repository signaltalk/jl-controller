import React, { Component } from 'react'
import connect from '../common/container'
import { Badge } from 'react-bootstrap'
import Button from './fast_button'
import { push } from 'react-router-redux'
import { game as game_actions } from '../game/actions'
import { control_channel as channel_actions } from '../channels/actions'
import { agari as agari_actions } from '../agari/actions'
import Immutable from 'immutable'

import Menu from './menu'

format_diff = (score) ->
  if score >= 0
    "+#{score}"
  else
    "#{score}"

export default Main = connect(
  ({ game, control_props, start, agari, ryukyoku, result, game_end }) ->
    { game, control_props, start, agari, ryukyoku, result, game_end }
  (dispatch) ->
    command: (args) -> dispatch channel_actions.command(args)
    push: (path) -> dispatch push path
    reach: -> dispatch game_actions.reach()
    reach_cancel: -> dispatch game_actions.reach_cancel()
    agari_tsumo: ->
      dispatch agari_actions.tsumo()
      dispatch push '/agari'
    agari_ron: (target) ->
      dispatch agari_actions.ron(target)
      dispatch push '/agari'
) class Main extends Component
  constructor: (props, context) ->
    super(props, context)
    @state = {
      st: do Immutable.Record(show_sub: false)
    }
  componentWillMount: -> @update_state @props
  componentWillReceiveProps: (nextProps) -> @update_state nextProps
  update_state: (props) ->
    switch
      when props.start?
        @props.push '/start'
      when props.ryukyoku?
        @props.push '/ryukyoku'
      when props.result?
        @props.push '/result'
      when props.agari?
        if props.agari.winner is 0
          @props.push '/agari'
      when props.game_end?
        @props.push '/end'
  show_sub: (show) -> @setState st: @state.st.set('show_sub', show)
  reach: ->
    @props.reach()
    @props.command type: 'GAME_REACH', id: @props.control_props.own_id
  reach_cancel: ->
    @props.reach_cancel()
    @props.command type: 'GAME_REACH_CANCEL', id: @props.control_props.own_id
  ryukyoku: ->
    @props.command type: 'RYUKYOKU'
  agari_ron: (target) ->
    @props.command
      type: 'AGARI'
      agari:
        type: 'RON'
        winner: @props.control_props.own_id
        target: @props.control_props.seat_to_id(target)
  agari_tsumo: (target) ->
    @props.command
      type: 'AGARI'
      agari:
        type: 'TSUMO'
        winner: @props.control_props.own_id
  render: ->
    { game, ryukyoku } = @props
    if not game?
      <p>loading...</p>
    else
      { st } = @state
      <div className="body main">
        <Menu className="menu-button" />
        <div className="table">
          <div className="table-row">
            <div className="table-front">
              <div className="score-board">
                { @seat(2) }
                { @score(1) }
                { @agari_button(2) }
              </div>
            </div>
          </div>
          <div className="table-row">
            <div className="table-left">
              <div className="score-board">
                { @agari_button(3) }
                { @score(3) }
                { @seat(3) }
              </div>
            </div>
            <div className="reach-block-vert">
              {
                if game.getIn(['reached', 3])
                  <span className="reach-image-vert">&nbsp;</span>
              }
            </div>
            <div className="table-center">
              <div className="reach-block">
                {
                  if game.getIn(['reached', 2])
                    <span className="reach-image">&nbsp;</span>
                }
              </div>
              <div className="info-frame">
                <p className="info-text kyoku">{ game.kyoku_name }</p>
                <p className="info-text">{ game.honba }本場</p>
                <p className="kyoutaku"><span className="reach-image">&nbsp;</span> × { game.current_kyoutaku }</p>
              </div>
              <div className="reach-block">
                {
                  if game.getIn(['reached', 0])
                    <span className="reach-image">&nbsp;</span>
                }
              </div>
            </div>
              <div className="reach-block-vert">
                {
                  if game.getIn(['reached', 1])
                    <span className="reach-image-vert">&nbsp;</span>
                }
              </div>
            <div className="table-right">
              <div className="score-board">
                { @seat(1) }
                { @score(1) }
                { @agari_button(1) }
              </div>
            </div>
          </div>
          <div className="table-row">
            <div className="table-me">
              <div className="score-board">
                { @seat(0) }
                { @score(0) }
                { @agari_button(0) }
              </div>
            </div>
          </div>
        </div>
        <div className="ui-buttons">
          {
            if @props.agari?
              <div className="btn">&nbsp;</div>
            else
              <Button bsStyle="default" onClick={ => @ryukyoku() }>流局</Button>
          }
          {
            if game.getIn(['reached', 0])
              <Button bsStyle="primary" className="reach-button" onClick={ => @reach_cancel() }>リーチキャンセル</Button>
            else
              <Button bsStyle="default" className="reach-button" onClick={ => @reach() }>リーチ</Button>
          }
        </div>
        <div className="dog-ear">
          {
            if st.show_sub
              <Button onClick={=> @show_sub(false)} active>点差</Button>
            else
              <Button onClick={=> @show_sub(true)}>点差</Button>
          }
        </div>
      </div>
  seat: (target) ->
    { st } = @state
    { game } = @props
    <div className="seat">
      {
        if st.show_sub
          game.rank(target)
        else
          if game.current_oya is target
            <div className="current_oya">東</div>
          else
            game.seat_name(target)
      }
    </div>
  agari_button: (target) ->
    <div className="agari-button">
      {
        switch
          when not @props.agari? and target is 0
            <Button onClick={=> @agari_tsumo()}>ツモ</Button>
          when not @props.agari? or (@props.agari?.type is 'RON' and target isnt 0 and @props.agari?.target is target)
            <Button onClick={=> @agari_ron(target)}>ロン</Button>
          when @props.agari?.winner is target or @props.agari?winners.include(target)
            <div className="btn dummy">点数申告中</div>
          else
            <div className="btn dummy"></div>
      }
    </div>
  score: (target) ->
    { st } = @state
    { game } = @props
    <div className="score">
      <p className="score-score">
        {
          if st.show_sub
            if target is 0
              '---'
            else
              format_diff game.score.get(0) - game.score.get(target)
          else
            game.score.get(target)
        }
      </p>
      <p className="score-chip">○×{ format_diff game.chip.get(target) }</p>
    </div>
