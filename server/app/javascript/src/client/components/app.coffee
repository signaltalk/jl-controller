import React, { Component } from 'react'
import connect from '../common/container'
import { ConnectedRouter, push } from 'react-router-redux'
import history from '../history'

import { Route, Switch } from 'react-router-dom'
import { control_channel as actions } from '../channels/actions'
import Config from '../config'

import Setup from './setup'
import Start from './start'
import Main from './main'
import Agari from './agari'
import Ryukyoku from './ryukyoku'
import Result from './result'
import GameEnd from './game_end'

export default App = connect(
  ({ control_props, control_channel }) -> { control_props, control_channel }
  (dispatch) ->
    subscribe: (control_props) -> dispatch actions.subscribe(control_props)
    command: (args) -> dispatch actions.command(args)
    push: (path) -> dispatch push path
) class App extends Component
  constructor: (props, context) ->
    super(props, context)
    @state = { connected: null }
  componentWillMount: ->
    @update_state @props
  componentWillReceiveProps: (nextProps) ->
    @update_state nextProps
  update_state: (nextProps) ->
    switch
      when not nextProps.control_props.own_id?
        @props.push '/setup'
      when not @state.connected?
        @props.subscribe(@props.control_props)
        @setState { connected: no }
      when not @state.connected and nextProps.control_channel.connected
        if Config.DEBUG_ON_SINGLE
          @props.command type: 'INIT', id: i for i in [1..4]
        else
          @props.command type: 'INIT', id: nextProps.control_props.own_id
        @setState { connected: yes }
  render: ->
    <ConnectedRouter history={history}>
      <Switch>
        <Route exact path='/' component={Console} />
        <Route path='/main' component={Main} />
        <Route path='/setup' component={Setup} />
        <Route path='/start' component={Start} />
        <Route path='/ryukyoku' component={Ryukyoku} />
        <Route path='/agari' component={Agari} />
        <Route path='/result' component={Result} />
        <Route path='/end' component={GameEnd} />
      </Switch>
    </ConnectedRouter>

Console = connect(
  ({ control_props, control_channel, start, game, ryukyoku }) ->
    {
      own_id: control_props.own_id
      connected: control_channel.connected
      start
      game
      ryukyoku
    }
  (dispatch) ->
    push: (path) -> dispatch push path
) class Console extends Component
  componentWillMount: ->
    @update_state @props
  componentWillReceiveProps: (nextProps) ->
    @update_state nextProps
  update_state: (props) ->
    switch
      when props.start?
        @props.push '/start'
      when props.ryukyoku?
        @props.push '/ryukyoku'
      when props.game?
        @props.push '/main'
  render: ->
    <div>
      <p>Initializing...</p>
      <p>own_id = {@props.own_id}</p>
      <p>{@props.connected}...</p>
    </div>
