import React, { Component, Fragment } from 'react'
import connect from '../common/container'
import Immutable from 'immutable'
import { Navbar, Tabs, Tab } from 'react-bootstrap'
import Button from './fast_button'
import { push } from 'react-router-redux'
import { control_channel as ch_actions } from '../channels/actions'
import { agari as agari_actions } from '../agari/actions'
import AGARI_TABLE from '../agari/agari_table'

HAN_NAMES = ['１飜', '２飜', '３飜', '４飜', '満貫～']

ImmutableRecord = (model) ->
  for k, v of model
    if v? and typeof v is 'object'
      model[k] = ImmutableRecord(v)
  do Immutable.Record(model)

export default Agari = connect(
  ({ control_props, agari, game, result }) ->
    { control_props, agari, game, result }
  (dispatch) ->
    push: (path) -> dispatch push path
    command: (payload) -> dispatch ch_actions.command(payload)
    agari_cancel: -> dispatch agari_actions.cancel()
) class Ryukyoku extends Component
  constructor: (props, context) ->
    super props, context
    @state = {
      st: ImmutableRecord {
        han: 2
        chip: 0
        score: null
        scroller:
          scores:
            up: false
            down: true
          chips:
            up: false
            down: true
      }
    }
  componentWillMount: -> @update_state @props
  componentWillReceiveProps: (nextProps) -> @update_state nextProps
  update_state: (props) ->
    switch
      when not props.agari?
        @props.push '/main'
      when props.result?
        @props.push '/result'

  mutate_state: (f) -> @setState st: @state.st.withMutations f
  select_han: (han) -> @mutate_state (st) =>
    st.set 'han', han
      .set 'score', null
      .setIn ['scroller', 'scores', 'up'], false
      .setIn ['scroller', 'scores', 'down'], @scores(han).length > 6
  select_score: (score) -> @mutate_state (st) -> st.set 'score', score
  select_chip: (chip) -> @mutate_state (st) -> st.set 'chip', chip
  scroll: (ref, h) -> ref.scrollTop += h
  on_scroll_scores: -> @mutate_state (st) =>
      ref = @refs.scores_scroller
      st.setIn ['scroller', 'scores', 'up'], ref.scrollTop > 0
        .setIn ['scroller', 'scores', 'down'], (ref.scrollHeight > ref.clientHeight) && (ref.scrollTop < ref.scrollHeight - ref.clientHeight)
  on_scroll_chips: -> @mutate_state (st) =>
    ref = @refs.chips_scroller
    st.setIn ['scroller', 'chips', 'up'], ref.scrollTop > 0
      .setIn ['scroller', 'chips', 'down'], (ref.scrollHeight > ref.clientHeight) && (ref.scrollTop < ref.scrollHeight - ref.clientHeight)
  agari_cancel: ->
    @props.command { type: 'AGARI_CANCEL' }
  submit: ->
    { st } = @state
    [_, sc...] = @scores(st.han)[st.score - 1]
    agari = {
      type: @props.agari.type
      winner: @props.control_props.own_id
      han: st.han
      selected: st.score
      score: sc
      chip: st.chip
    }
    agari.target = @props.control_props.seat_to_id(@props.agari.target) if agari.type is 'RON'
    @props.command { type: 'AGARI_OK', agari }
  render: ->
    if not @props.agari?
      <p>loading...</p>
    else
      { st } = @state
      <div className="body agari">
        <Navbar>
          <Navbar.Header>
            <h1>{@title()}</h1>
          </Navbar.Header>
        </Navbar>
        <div className="agari-body">
          <div className="han-tabs">
            {
              on_select_han = (i) => => @select_han(i)
              for han_name, i in HAN_NAMES
                i += 1
                a =
                  if st.han is i
                    { bsStyle: "primary", active: true }
                  else
                    { onClick: on_select_han(i) }
                <Button className="han-button" key={i} {a...}>{han_name}</Button>
            }
          </div>
          <div className="body-inner">
            <div className="scores">
              <div className="scroller-wrapper">
                {
                  if st.scroller.scores.up
                    <Button className="scroller up" onClick={=> @scroll(@refs.scores_scroller, -180)}  >△</Button>
                }
                <div className="scores-scroller" ref="scores_scroller" onScroll={=> @on_scroll_scores()}>
                  <div className="scores-inner">
                    {
                      on_select_score = (i) => => @select_score(i)
                      for tbl, i in @scores(st.han)
                        [label, sc...] = tbl
                        i += 1
                        a =
                          if st.score is i
                            { bsStyle: "primary", active: true }
                          else
                            { onClick: on_select_score(i) }
                        <Button key={i} className="score-button" {a...}>
                          <div className="score-label">{label}</div>
                          <div className="score-score">
                            {
                              switch
                                when @props.agari.type is 'RON'
                                  sc[0]
                                when @props.game.current_oya is 0
                                  "#{sc[0]}オール"
                                else
                                  "#{sc[0]} / #{sc[1]}"
                            }
                          </div>
                        </Button>
                    }
                  </div>
                </div>
                {
                  if st.scroller.scores.down
                    <Button className="scroller down" onClick={=> @scroll(@refs.scores_scroller, 180)}  >▽</Button>
                }
              </div>
            </div>
            <div className="chips">
              <div className="chips-label">チップ</div>
              <div className="scroller-wrapper">
                {
                  if st.scroller.chips.up
                    <Button className="scroller up" onClick={=>@scroll(@refs.chips_scroller, -122)}>△</Button>
                }
                <div className="chips-scroller" ref="chips_scroller" onScroll={=> @on_scroll_chips()}>
                  <div className="chips-inner">
                    {
                      on_select_chip = (i) => => @select_chip(i)
                      for i in [0...21+1]
                        a =
                          if st.chip is i
                            { bsStyle: "primary", active: true }
                          else
                            { onClick: on_select_chip(i) }
                        <Button key={i} className="chip-button" {a...}>{i}</Button>
                    }
                  </div>
                </div>
                {
                  if st.scroller.chips.down
                    <Button className="scroller down" onClick={=> @scroll(@refs.chips_scroller, 122)}>▽</Button>
                }
              </div>
            </div>
          </div>
        </div>
        <div className="footer submit-buttons">
          <Button onClick={=> @agari_cancel()}>戻る</Button>
          {
            attrs = 
              if st.score?
                { bsStyle: 'primary', onClick: => @submit() }
              else
                { disabled: true }
            <Button bsSize="large" {attrs...}>次へ</Button>
          }
        </div>
      </div>
  title: ->
    console.log @props.agari.toJS(), @props.agari.winner_name
    "#{@props.game.kyoku_name}#{@props.game.honba}本場
     #{@props.agari.winner_name}家 #{@props.agari.type_name}
     #{if @props.agari.type is 'RON' then "(←#{@props.agari.target_name}家)" else '' }"
  scores: (han) ->
    AGARI_TABLE[@props.agari.type][if @props.game.current_oya is 0 then 0 else 1][han - 1]
