import Immutable from 'immutable'
import typeToReducer from 'type-to-reducer'

export default reducers =
  result:
    typeToReducer
      RESULT:
        DISCARD: (result) -> null
      CONTROL_CHANNEL:
        RECEIVED: (state, {control_props, payload}) ->
          switch payload.type
            when 'RESULT'
              {type, result...} = payload
              control_props.transform_result Immutable.fromJS(result)
            else
              null
      null
