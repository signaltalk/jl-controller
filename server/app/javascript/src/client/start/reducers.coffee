import Immutable from 'immutable'
import typeToReducer from 'type-to-reducer'

export default reducers =
  start:
    typeToReducer
      CONTROL_CHANNEL:
        RECEIVED: (start, {control_props, payload}) ->
          switch payload.type
            when 'GAME_START'
              control_props.transform_start(
                Immutable.fromJS(payload)
              )
            when 'GAME'
              null
            else
              start
      null
