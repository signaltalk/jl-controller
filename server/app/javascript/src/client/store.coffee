import { createStore, compose, combineReducers, applyMiddleware } from 'redux'
import ReduxThunk from 'redux-thunk'
import promiseMiddleware from 'redux-promise-middleware'
import { routerReducer, routerMiddleware } from 'react-router-redux'

import reducers from './reducers'
reducer = combineReducers({ reducers..., router: routerReducer })

import history from './history'
enhancer = applyMiddleware(
  routerMiddleware(history)
  ReduxThunk
  promiseMiddleware()
)
enhancer = compose enhancer, window.devToolsExtension() if window.devToolsExtension?
export default store = createStore reducer, enhancer
