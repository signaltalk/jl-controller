import Immutable from 'immutable'
import typeToReducer from 'type-to-reducer'
import GameEnd from './game_end'

export default reducers =
  game_end:
    typeToReducer
      CONTROL_CHANNEL:
        RECEIVED: (game_end, {control_props, payload}) ->
          switch payload.type
            when 'GAME_END'
              control_props.transform_game_end new GameEnd(Immutable.fromJS(payload))
            when 'GAME'
              null
            else
              game_end
      null

