import Immutable from 'immutable'
import '../common/property'

Record = Immutable.Record
  rank: Immutable.List()
  score: Immutable.List()
  chip: Immutable.List()
  pt: do Immutable.Record
    rank: Immutable.List()
    score: Immutable.List()
    chip: Immutable.List()
  ok: Immutable.Map()
  should_cancel: true

export default class GameEnd extends Record
  pt_total: (i) ->
    @getIn(['pt', 'rank', i]) + @getIn(['pt', 'score', i]) + @getIn(['pt', 'chip', i])
