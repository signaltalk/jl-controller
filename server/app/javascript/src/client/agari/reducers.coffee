import Immutable from 'immutable'
import typeToReducer from 'type-to-reducer'
import Agari from './agari'

export default reducers =
  agari:
    typeToReducer
      CONTROL_CHANNEL:
        RECEIVED: (agari, {control_props, payload}) ->
          switch payload.type
            when 'AGARI'
              control_props.transform_agari new Agari(Immutable.fromJS(payload.agari))
            when 'AGARI_CANCEL', 'NEXT_GAME', 'GAME_END'
              null
            else
              agari
      null
