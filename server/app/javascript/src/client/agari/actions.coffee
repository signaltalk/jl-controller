export ActionTypes =
  AGARI:
    TSUMO: 'AGARI_TSUMO'
    RON: 'AGARI_RON'
    CANCEL: 'AGARI_CANCEL'

export agari =
  tsumo: -> { type: ActionTypes.AGARI.TSUMO }
  ron: (target) -> { type: ActionTypes.AGARI.RON, target }
  cancel: -> { type: ActionTypes.AGARI.CANCEL }

export default { agari }
