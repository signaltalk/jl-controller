export default AGARI_TABLE =
  {
    TSUMO: [
      [
        [
          ['30符', 500]
          ['40符', 700]
          ['50符', 800]
          ['60符', 1000]
          ['70符', 1200]
          ['80符', 1300]
          ['90符', 1500]
          ['100符', 1600]
          ['110符', 1800]
        ]
        [
          ['平和', 700]
          ['30符', 1000]
          ['40符', 1300]
          ['50符', 1600]
          ['60符', 2000]
          ['70符', 2300]
          ['80符', 2600]
          ['90符', 2900]
          ['100符', 3200]
          ['110符', 3600]
        ]
        [
          ['平和', 1300]
          ['七対子', 1600]
          ['30符', 2000]
          ['40符', 2600]
          ['50符', 3200]
          ['満貫', 4000]
        ]
        [
          ['平和', 2600]
          ['七対子', 3200]
          ['満貫', 4000]
        ]
        [
          ['満貫', 4000]
          ['跳満', 6000]
          ['倍満', 8000]
          ['三倍満', 12000]
          ['役満', 16000]
          ['ダブル役満', 32000]
        ]
      ]
      [
        [
          ['30符', 300, 500]
          ['40符', 400, 700]
          ['50符', 400, 800]
          ['60符', 500, 1000]
          ['70符', 600, 1200]
          ['80符', 700, 1300]
          ['90符', 800, 1500]
          ['100符', 800, 1600]
          ['110符', 900, 1800]
        ]
        [
          ['平和', 400, 700]
          ['30符', 500, 1000]
          ['40符', 700, 1300]
          ['50符', 800, 1600]
          ['60符', 1000, 2000]
          ['70符', 1200, 2300]
          ['80符', 1300, 2600]
          ['90符', 1500, 2900]
          ['100符', 1600, 3200]
          ['110符', 1800, 3600]
        ]
        [
          ['平和', 700, 1300]
          ['七対子', 800, 1600]
          ['30符', 1000, 2000]
          ['40符', 1300, 2600]
          ['50符', 1600, 3200]
          ['満貫', 2000, 4000]
        ]
        [
          ['平和', 1300, 2600]
          ['七対子', 1600, 3200]
          ['満貫', 2000, 4000]
        ]
        [
          ['満貫', 2000, 4000]
          ['跳満', 3000, 6000]
          ['倍満', 4000, 8000]
          ['三倍満', 6000, 12000]
          ['役満', 8000, 16000]
          ['ダブル役満', 16000, 32000]
        ]
      ]
    ]
    RON: [
      [
        [
          ['30符', 1500]
          ['40符', 2000]
          ['50符', 2400]
          ['60符', 2900]
          ['70符', 3400]
          ['80符', 3900]
          ['90符', 4400]
          ['100符', 4800]
          ['110符', 5300]
        ]
        [
          ['七対子', 2400]
          ['30符', 2900]
          ['40符', 3900]
          ['50符', 4800]
          ['60符', 5800]
          ['70符', 6800]
          ['80符', 7700]
          ['90符', 8700]
          ['100符', 9600]
          ['110符', 10600]
        ]
        [
          ['七対子', 4800]
          ['30符', 5800]
          ['40符', 7700]
          ['50符', 9600]
          ['満貫', 12000]
        ]
        [
          ['七対子', 9600]
          ['満貫', 12000]
        ]
        [
          ['満貫', 12000]
          ['跳満', 18000]
          ['倍満', 24000]
          ['三倍満', 36000]
          ['役満', 48000]
          ['ダブル役満', 96000]
        ]
      ]
      [
        [
          ['30符', 1000]
          ['40符', 1300]
          ['50符', 1600]
          ['60符', 2000]
          ['70符', 2300]
          ['80符', 2600]
          ['90符', 2900]
          ['100符', 3200]
          ['110符', 3600]
        ]
        [
          ['七対子', 1600]
          ['30符', 2000]
          ['40符', 2600]
          ['50符', 3200]
          ['60符', 3900]
          ['70符', 4500]
          ['80符', 5200]
          ['90符', 5800]
          ['100符', 6400]
          ['110符', 7100]
        ]
        [
          ['七対子', 3200]
          ['30符', 3900]
          ['40符', 5200]
          ['50符', 6400]
          ['満貫', 7700]
        ]
        [
          ['七対子', 6400]
          ['満貫', 8000]
        ]
        [
          ['満貫', 8000]
          ['跳満', 12000]
          ['倍満', 16000]
          ['三倍満', 24000]
          ['役満', 32000]
          ['ダブル役満', 64000]
        ]
      ]
    ]
  }

