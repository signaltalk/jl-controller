import Immutable from 'immutable'
import '../common/property'

import store from '../store'

AgariRecord = Immutable.Record
  type: 'RON'
  winner: null
  target: null

export default class Agari extends AgariRecord
  @property 'type_name', get: ->
    switch @type
      when 'RON' then 'ロンあがり'
      when 'TSUMO' then 'ツモあがり'
  @property 'winner_name', get: -> @game().seat_name(@winner)
  @property 'target_name', get: -> @game().seat_name(@target)
  @property 'state', get: -> store.getState()
  game: ->
    @state.control_props.transform_game(@state.game)
