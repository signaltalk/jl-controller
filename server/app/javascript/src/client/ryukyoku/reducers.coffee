import Immutable from 'immutable'
import typeToReducer from 'type-to-reducer'

export default reducers =
  ryukyoku:
    typeToReducer
      CONTROL_CHANNEL:
        RECEIVED: (state, {control_props, payload}) ->
          switch payload.type
            when 'RYUKYOKU'
              Immutable.fromJS(
                selected: control_props.id_to_seat(payload.selected)
              )
            else
              null
      null
