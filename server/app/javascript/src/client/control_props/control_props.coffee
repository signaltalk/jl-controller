import Immutable from 'immutable'
import Game from '../game/game.coffee'

ControlPropsRecord = Immutable.Record
  own_id: null
  connected: new Immutable.List

export default class ControlProps extends ControlPropsRecord
  id_to_seat: (id) -> (4 + id - @own_id) % 4
  seat_to_id: (seat) -> (4 + seat + @own_id - 1) % 4 + 1
  transform_list: (list) ->
    if list?
      Immutable.List.of(
        list.get(@seat_to_id(0) - 1)
        list.get(@seat_to_id(1) - 1)
        list.get(@seat_to_id(2) - 1)
        list.get(@seat_to_id(3) - 1)
      )
    else
      list
  transform_start: (start) ->
    start.update 'random', (i) => i && @id_to_seat(i)
         .update 'selected', (i) => i && @id_to_seat(i)
         .update 'ok', (a) => a && a.map (i) => @id_to_seat(i)
  transform_game: (game) ->
    game.withMutations (st) =>
      st.update 'start_seat', (i) => @id_to_seat(i)
        .update 'score', (a) => @transform_list(a)
        .update 'chip', (a) => @transform_list(a)
        .update 'reached', (a) => @transform_list(a)
        .update 'kyoutakued', (a) => @transform_list(a)
  transform_result: (result) ->
    game = new Game(result.get('game'))
    game = @transform_game(game)
    result.withMutations (st) =>
      st.set 'game', game
        .update 'subject', (subject) =>
          if result.type is 'agari'
            subject.update 'winner', (i) => @id_to_seat(i)
                   .update 'target', (i) => @id_to_seat(i)
          else
            subject
        .updateIn ['diff', 'score'], (a) => @transform_list(a)
        .updateIn ['diff', 'chip'], (a) => @transform_list(a)
        .update 'ok', (a) => a && Immutable.Map(a.mapKeys (k) => @id_to_seat(k))
  transform_agari: (agari) ->
    agari.withMutations (st) =>
      st.update 'winner', (v) => @id_to_seat(v)
        .update 'target', (v) => @id_to_seat(v)
  transform_game_end: (game_end) ->
    game_end.withMutations (st) =>
      st.update 'rank', (a) => @transform_list(a)
        .update 'score', (a) => @transform_list(a)
        .update 'chip', (a) => @transform_list(a)
        .updateIn ['pt', 'rank'], (a) => @transform_list(a)
        .updateIn ['pt', 'score'], (a) => @transform_list(a)
        .updateIn ['pt', 'chip'], (a) => @transform_list(a)
        .update 'ok', (a) => a && Immutable.Map(a.mapKeys (k) => @id_to_seat(k))

