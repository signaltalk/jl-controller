
export ActionTypes =
  CONTROL_PROPS:
    SETUP: 'CONTROL_PROPS_SETUP'

export control_props =
  setup: (props) -> {type: ActionTypes.CONTROL_PROPS.SETUP, props}

export default {control_props}
