import typeToReducer from 'type-to-reducer'
import ControlProps from './control_props'

export default reducers =
  control_props:
    typeToReducer
      CONTROL_PROPS:
        SETUP: (control_props, {type, props}) ->
          control_props = control_props.merge props
          p = control_props.toJS()
          json = JSON.stringify({own_id: p.own_id})
          console.log json
          localStorage.setItem 'control_props', json
          control_props
      CONTROL_CHANNEL:
        RECEIVED: (control_props, payload) ->
          switch payload.type
            when 'INIT'
              control_props.set 'connected', payload.connected
            else
              control_props
      do ->
        json = localStorage.getItem('control_props')
        if json?
          new ControlProps(JSON.parse(json))
        else
          new ControlProps
