import Immutable from 'immutable'
import typeToReducer from 'type-to-reducer'
import Game from './game'

export default reducers =
  game:
    typeToReducer
      GAME:
        REACH: (game) ->
          game.setIn(['reached', 0], true)
              .updateIn(['score', 0], (s) -> s - 1000)
        REACH_CANCEL: (game) ->
          game.setIn(['reached', 0], false)
              .updateIn(['score', 0], (s) -> s + 1000)
        KYOUTAKU: (game) ->
          game.updateIn(['kyoutakued', 0], (s) -> s + 1)
              .updateIn(['score', 0], (s) -> s - 1000)
        KYOUTAKU_CANCEL: (game) ->
          game.updateIn(['kyoutakued', 0], (s) -> s - 1)
              .updateIn(['score', 0], (s) -> s + 1000)
      CONTROL_CHANNEL:
        RECEIVED: (game, {control_props, payload}) ->
          switch payload.type
            when 'GAME', 'NEXT_GAME'
              control_props.transform_game new Game(Immutable.fromJS(payload))
            else
              game
      null

