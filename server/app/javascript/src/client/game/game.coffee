import Immutable from 'immutable'
import '../common/property'

GameRecord = Immutable.Record
  start_seat: null
  kyoku: 0
  honba: 0
  kyoutaku: 0
  score: Immutable.List([25000, 25000, 25000, 25000])
  chip: Immutable.List([10, 10, 10, 10])
  reached: Immutable.List([false, false, false, false])
  kyoutakued: Immutable.List([0, 0, 0, 0])

export default class Game extends GameRecord
  @property 'kyoku_name', get: ->
    if @kyoku < 1
      '東零局'
    else
      ba = if @kyoku <= 4 then '東' else '南'
      kyoku_s = ['一', '二', '三', '四']
      k = (@kyoku - 1) % 4
      "#{ba}#{kyoku_s[k]}局"
  @property 'current_oya', get: -> (@start_seat + @kyoku - 1) % 4
  @property 'current_kyoutaku', get: -> (@kyoutaku + @kyoutakued.reduce((a, v) -> a + v))
  seat_name: (target) ->
    s = ['東', '南', '西', '北']
    s[(target - @current_oya + 4) % 4]
  rank: (target) ->
    seats = Immutable.List([0, 1, 2, 3]).map((i) => (i + @current_oya) % 4)
    seats = seats.sort((a, b) => @score.get(a) < @score.get(b))
    seats.indexOf(target) + 1





