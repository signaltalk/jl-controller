export ActionTypes =
  GAME:
    START: 'GAME_START'
    REACH: 'GAME_REACH'
    REACH_CANCEL: 'GAME_REACH_CANCEL'
    KYOUTAKU: 'GAME_KYOUTAKU'
    KYOUTAKU_CANCEL: 'GAME_KYOUTAKU_CANCEL'

export game =
  reach: -> {type: ActionTypes.GAME.REACH}
  reach_cancel: -> {type: ActionTypes.GAME.REACH_CANCEL}
  kyoutaku: -> { type: ActionTypes.GAME.KYOUTAKU }
  kyoutaku_cancel: -> { type: ActionTypes.GAME.KYOUTAKU_CANCEL }

export default {game}
