import Immutable from 'immutable'

export default ImmutableRecord = (src) ->
  model = {}
  for k, v of src
    model[k] =
      if v? and typeof v is 'object'
        ImmutableRecord(v)
      else
        v
  do Immutable.Record(model)
