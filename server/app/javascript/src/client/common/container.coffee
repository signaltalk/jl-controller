import { connect as _connect } from 'react-redux'

export default connect = (args...) ->
  (component) ->
    c = _connect(args...) component
    name = component.name
    c[name] = component if name
    c
