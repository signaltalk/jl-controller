import typeToReducer from 'type-to-reducer'
import ControlChannel from './control_channel'

export default reducers =
  control_channel:
    typeToReducer
      CONTROL_CHANNEL:
        SUBSCRIBE: (channel, {type, args...}) ->
          channel.subscribe args
        CONNECTED: (channel) ->
          channel.set 'connected', yes
        RECEIVED: (channel, {type, control_props, payload}) ->
          console.log 'control_channel: received ->', payload
          channel
        COMMAND: (channel, {payload}) ->
          console.log 'control_channel: command ->', payload
          channel.command payload
          channel
      GAME:
        START: (channel, {start_seat}) ->
          channel.command { type: 'GAME_START', start_seat }
          channel
      new ControlChannel
