
export ActionTypes =
  CONTROL_CHANNEL: {
    SUBSCRIBE: 'CONTROL_CHANNEL_SUBSCRIBE'
    CONNECTED: 'CONTROL_CHANNEL_CONNECTED'
    DISCONNECTED: 'CONTROL_CHANNEL_DISCONNECTED'
    REJECTED: 'CONTROL_CHANNEL_REJECTED'
    RECEIVED: 'CONTROL_CHANNEL_RECEIVED'
    COMMAND: 'CONTROL_CHANNEL_COMMAND'
  }

export control_channel =
  subscribe: (control_props) ->
    (dispatch) ->
      dispatch {
        type: ActionTypes.CONTROL_CHANNEL.SUBSCRIBE
        connected: -> dispatch type: ActionTypes.CONTROL_CHANNEL.CONNECTED
        disconnected: -> dispatch type: ActionTypes.CONTROL_CHANNEL.DISCONNECTED
        rejected: -> dispatch type: ActionTypes.CONTROL_CHANNEL.REJECTED
        received: (payload) -> dispatch { type: ActionTypes.CONTROL_CHANNEL.RECEIVED, control_props, payload }
      }
  command: (payload) -> {type: ActionTypes.CONTROL_CHANNEL.COMMAND, payload}

export default {control_channel}
