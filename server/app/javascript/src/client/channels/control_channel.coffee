import Immutable from 'immutable'

ControlChannelRecord = Immutable.Record
  channel: null
  connected: no

export default class ControlChannel extends ControlChannelRecord
  subscribe: (args) ->
    @set 'channel', App.control args

  command: (payload) ->
    @channel.command payload

  disconnect: ->
    @channel.consumer.disconnect()
