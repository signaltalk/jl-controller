#
class MainController < ApplicationController
  def index() end

  def publish
    require 'mqtt'

    MQTT::Client.connect(host: 'broker', port: 1883) do |client|
      client.publish 'topic/test', 'this is test!'
    end
  end
end
