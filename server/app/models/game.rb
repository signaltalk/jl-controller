#
class Game
  include RedisAttributes
  include Start
  include Agari
  include Ryukyoku
  include Result
  include GameEnd
  include Chonbo

  REQUIRE_CONNECTED = 4
  LAST_KYOKU = 8
  INIT_SCORE = 25_000

  def status
    get('status')
  end

  def init(id)
    stat = with('status') do |s|
      connected = Set.new(s['connected'] || [])
      connected << id
      { type: 'INIT', connected: connected }
    end
    if stat[:connected].size >= REQUIRE_CONNECTED
      g = current_game
      set 'status', g
      g
    else
      stat
    end
  end

  def restart_game
    with('game') { start }
  end

  def current_game
    with('game') do |g|
      g || start
    end
  end

  def reach(id)
    with('game') do |g|
      unless g['reached'][id - 1]
        g['reached'][id - 1] = true
        g['score'][id - 1] -= 1000
      end
      g
    end
  end

  def reach_cancel(id)
    with('game') do |g|
      if g['reached'][id - 1]
        g['reached'][id - 1] = false
        g['score'][id - 1] += 1000
      end
      g
    end
  end

  def kyoutaku(id)
    with('game') do |g|
      g['kyoutakued'][id - 1] += 1
      g['score'][id - 1] -= 1000
      g
    end
  end

  def kyoutaku_cancel(id)
    with('game') do |g|
      if g['kyoutakued'][id - 1].positive?
        g['kyoutakued'][id - 1] -= 1
        g['score'][id - 1] += 1000
      end
      g
    end
  end

  private

  def new_game(start_seat)
    {
      type: 'GAME',
      start_seat: start_seat,
      kyoku: 1,
      honba: 0,
      kyoutaku: 0,
      score: Array.new(4, INIT_SCORE),
      chip: Array.new(4, 0),
      reached: Array.new(4, false),
      kyoutakued: Array.new(4, 0)
    }
  end

  def next_kyoku(result)
    g = with('game') do |game|
      diff = result['diff']
      diff['score'].each_with_index { |s, i| game['score'][i] += s } if diff.key?('score')
      diff['chip'].each_with_index { |s, i| game['chip'][i] += (s || 0) } if diff.key?('chip')
      if result['context'] != 'chonbo'
        game['kyoutaku'] += game['kyoutakued'].inject(:+)
        if diff.key?('kyoutaku')
          game['kyoutaku'] += diff['kyoutaku']
          game['kyoutaku'] = 0 if game['kyoutaku'].negative?
        end
        if renchan?(result, game)
          game['honba'] += 1
        else
          game['kyoku'] += 1
          case result['context']
          when 'agari'
            game['honba'] = 0
          when 'ryukyoku'
            game['honba'] += 1
          end
        end
      end
      game['reached'] = Array.new(4, false)
      game['kyoutakued'] = Array.new(4, 0)
      game
    end
    if g['kyoku'] > LAST_KYOKU
      build_game_end(g)
    else
      g['type'] = 'NEXT_GAME'
      g
    end
  end

  def renchan?(result, game)
    current_oya = (game['start_seat'] - 1 + game['kyoku'] - 1) % 4 + 1
    seats = (0..3).map { |i| (game['start_seat'] - 1 + i) % 4 + 1 }
    winners = result['diff']['score'] ? seats.select { |i| result['diff']['score'][i - 1].positive? } : []
    if result['context'] == 'ryukyoku'
      if result['subject'] == 'qua-reach'
        true
      elsif winners.include?(current_oya)
        rank_id = seats.sort { |a, b| game['score'][b - 1] <=> game['score'][a - 1] }
        game['kyoku'] < LAST_KYOKU || rank_id.first != current_oya
      end
    elsif result['context'] == 'agari'
      winners.first == current_oya
    end
  end
end
