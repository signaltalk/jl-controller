class Game
  #
  module GameEnd
    REQUIRE_OK = 2
    # UMAOKA = [20 + 20, 10, -10, -20]
    # ORIGIN_SCORE = 30_000
    # CHIP_RATE = 2
    # SCORE_UNIT = 100
    UMAOKA = [30, 10, -10, -30].freeze
    ORIGIN_SCORE = 25_000
    CHIP_RATE = 0
    SCORE_UNIT = 1000

    def game_end
      with('status') {
        g = current_game
        reached_count = g['reached'].select { |v| v }.size
        g['kyoutaku'] += reached_count
        build_game_end(g, true)
      }
    end

    def round1000(s)
      n = s / 1000.0
      if n % 1.0 >= 0.5
        n.floor + 1
      else
        n.floor
      end
    end

    def build_game_end(game, should_cancel = false)
      rank_id = (1..4).map { |i| (game['start_seat'] - 1 + i - 1) % 4 + 1 }
                      .sort { |a, b| game['score'][b - 1] <=> game['score'][a - 1] }
      reached_count = game['reached'].select { |v| v }.size
      current_kyoutaku = game['kyoutaku'] + game['kyoutakued'].inject(:+)
      game['score'][rank_id[0] - 1] += (reached_count + current_kyoutaku) * 1000
      rank = (1..4).map { |i| rank_id.index(i) + 1 }

      pt_score =
        if SCORE_UNIT == 100
          game['score'].map { |s| ((s - ORIGIN_SCORE) / 1000.0).round(1) }
        else
          game['score'].map { |s| round1000(s - ORIGIN_SCORE) }
        end
      pt_score[rank_id[0] - 1] = (1..4).reject { |i| i == rank_id[0] }
                                       .map { |i| -pt_score[i - 1] }
                                       .inject(:+)

      pt_rank = rank.map { |i| UMAOKA[i - 1] }
      pt_chip = game['chip'].map { |s| s * CHIP_RATE }
      {
        type: 'GAME_END',
        rank: rank,
        score: game['score'],
        chip: game['chip'],
        pt: {
          rank: pt_rank,
          score: pt_score,
          chip: pt_chip
        },
        ok: { '1' => false, '2' => false, '3' => false, '4' => false },
        should_cancel: should_cancel
      }
    end

    def game_end_ok(id, ok)
      stat = with('status') do |s|
        if s['type'] == 'GAME_END'
          s['ok'][id.to_s] = ok
        end
        s
      end
      if stat['type'] == 'GAME_END'
        ok_count = stat['ok'].values.select { |v| v }.size
        if ok_count >= REQUIRE_OK
          stat = start
        end
      end
      stat
    end

    def game_end_cancel
      with('status') do |s|
        if s['type'] == 'GAME_END'
          s = current_game if s['should_cancel']
        end
        s
      end
    end
  end
end
