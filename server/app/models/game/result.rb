class Game
  #
  module Result
    def result(context, subject, diff, ok, require_ok)
      with('result') do
        game = current_game
        {
          type: 'RESULT',
          context: context,
          subject: subject,
          game: current_game,
          diff: diff,
          ok: ok,
          require_ok: require_ok
        }
      end
    end

    def result_ok(id, ok = true)
      id = id.to_s
      r = with('result') do |s|
        if s['type'] == 'RESULT'
          s['ok'][id] = ok if s['ok'].key?(id)
        end
        s
      end
      if r['type'] == 'RESULT'
        ok_count = r['ok'].values.select { |v| v }.size
        if ok_count >= r['require_ok']
          r = with('status') { next_kyoku(r) }
        end
      end
      r
    end
  end
end
