class Game
  #
  module Ryukyoku
    REQUIRE_SELECT = 4
    REQUIRE_OK = 2
    REQUIRE_OK_NO_WINNER = 2
    TENPAI_FEE = 3000

    def ryukyoku(selected = nil)
      with('status') do |s|
        if selected
          ryukyoku_result_no_winner(selected)
        elsif s['type'] == 'RYUKYOKU'
          s
        else
          { type: 'RYUKYOKU' }
        end
      end
    end

    def ryukyoku_selected(id, selected)
      if selected == 'tenpai'
        ryukyoku_tenpai(id, true)
      elsif selected == 'noten'
        ryukyoku_tenpai(id, false)
      elsif selected.nil?
        ryukyoku_tenpai(id, nil)
      else
        ryukyoku(selected)
      end
    end

    def ryukyoku_tenpai(id, tenpai)
      stat = with('status') do |s|
        selected = s['selected'] || {}
        if tenpai.nil?
          selected.delete(id.to_s)
        else
          selected[id.to_s] = tenpai
        end
        { type: 'RYUKYOKU', selected: selected }
      end
      if stat[:selected].size >= REQUIRE_SELECT
        ryukyoku_result(stat[:selected])
      else
        stat
      end
    end

    def ryukyoku_cancel
      with('status') do
        current_game
      end
    end

    def ryukyoku_result(selected)
      diff = {}
      tenpai = selected.select { |_, v| v }.keys
      if tenpai.size.positive? && tenpai.size < 4
        diff['score'] = (1..4).map { |i| TENPAI_FEE / (selected[i.to_s] ? tenpai.size : tenpai.size - 4) }
        ok = (1..4).reject { |i| selected[i.to_s] }
                   .map { |i| [i.to_s, false] }
                   .to_h
        require_ok = [ok.size, REQUIRE_OK].min
      else
        ok = (1..4).map { |i| [i.to_s, false] }.to_h
        require_ok = REQUIRE_OK_NO_WINNER
      end
      build_ryukyoku_result('ryukyoku', diff, ok, require_ok)
    end

    def ryukyoku_result_no_winner(subject)
      ok = (1..4).map { |i| [i.to_s, false] }.to_h
      build_ryukyoku_result(subject, {}, ok, REQUIRE_OK_NO_WINNER)
    end

    def build_ryukyoku_result(subject, diff, ok, require_ok)
      game = current_game
      reached_count = game['reached'].select { |v| v }.size
      diff['kyoutaku'] = reached_count if reached_count.positive?
      result('ryukyoku', subject, diff, ok, require_ok)
    end
  end
end
