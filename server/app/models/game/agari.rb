class Game
  #
  module Agari
    REQUIRE_OK_ON_TSUMO = 2
    HONBA = 100

    def agari(agari)
      with('status') do |s|
        {
          type: 'AGARI',
          agari: agari
        }
      end
    end

    def agari_cancel
      with('status') do
        {
          type: 'AGARI_CANCEL'
        }
      end
    end

    def agari_ok(agari)
      with('status') do |s|
        agari_result(agari)
      end
    end

    #
    # agari:
    #   type: 'RON' or 'TSUMO'
    #   winner: [N...]
    #   target: N
    #   selected: {
    #     'N' => {
    #       han: N
    #       selected: N
    #       score: [N, N]
    #     }
    #   }
    #
    def agari_result(agari)
      game = current_game
      current_oya = (game['start_seat'] - 1 + game['kyoku'] - 1) % 4 + 1
      subject = {
        'type' => agari['type'],
        'winner' => agari['winner'],
        'target' => agari['target'],
        'han' => agari['han'],
        'selected' => agari['selected']
      }
      diff = {}
      if agari['type'] == 'RON'
        diff['score'] = (1..4).map do |i|
          if i == agari['winner']
            agari['score'].first + game['honba'] * HONBA * 3
          elsif i == agari['target']
            -(agari['score'].first + game['honba'] * HONBA * 3)
          else
            0
          end
        end
      elsif agari['winner'] == current_oya
        diff['score'] = (1..4).map do |i|
          if i == agari['winner']
            agari['score'].first * 3 + game['honba'] * HONBA * 3
          else
            -(agari['score'].first + game['honba'] * HONBA)
          end
        end
      else
        diff['score'] = (1..4).map do |i|
          if i == agari['winner']
            agari['score'].first * 2 + agari['score'].second + game['honba'] * HONBA * 3
          elsif i == current_oya
            -(agari['score'].second + game['honba'] * HONBA)
          else
            -(agari['score'].first + game['honba'] * HONBA)
          end
        end
      end
      if agari['chip'] > 0
        diff['chip'] = (1..4).map do |i|
          if agari['type'] == 'RON'
            if i == agari['winner']
              agari['chip']
            elsif i == agari['target']
              -agari['chip']
            end
          else
            if i == agari['winner']
              agari['chip'] * 3
            else
              -agari['chip']
            end
          end
        end
      end
      reached_count = game['reached'].select { |v| v }.size
      current_kyoutaku = game['kyoutaku'] + game['kyoutakued'].inject(:+) + reached_count
      diff['kyoutaku'] = -current_kyoutaku
      diff['score'][agari['winner'] - 1] += current_kyoutaku * 1000
      ok =
        if agari['type'] == 'RON'
          require_ok = 1
          {
            agari['target'].to_s => false
          }
        else
          require_ok = REQUIRE_OK_ON_TSUMO
          (1..4).reject { |i| i == agari['winner'] }
                .map { |i| [i.to_s, false] }
                .to_h
        end
      result('agari', subject, diff, ok, require_ok)
    end
  end
end
