class Game
  #
  module RedisAttributes
    def redis_key(key)
      "#{self.class.name}.#{key}"
    end

    def get(key)
      Redis.current.with do |redis|
        data = redis.get(redis_key(key))
        if data
          JSON.parse(data)
        else
          initial_data[key]
        end
      end
    end

    def set(key, value)
      Redis.current.with do |redis|
        redis.set redis_key(key), value.to_json
      end
    end

    def lock(key)
      Rails.logger.debug "lock on (#{key})"
      r = Redis::Lock.new(key, expiration: 15, timeout: 1).lock { yield }
      Rails.logger.debug "release lock on (#{key})"
      r
    end

    def with(key)
      key = key.to_sym
      Redis.current.with do |redis|
        rkey = redis_key(key)
        lkey = "#{rkey}#lock"
        lock(lkey) do
          data = redis.get(rkey)
          value =
            if data
              JSON.parse(data)
            else
              initial_data[key]
            end
          value = yield value
          redis.set rkey, value.to_json
          value
        end
      end
    end

    def initial_data
      {
        status: { type: 'INIT', connected: [] }
      }
    end
  end
end
