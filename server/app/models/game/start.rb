class Game
  #
  module Start
    REQUIRE_OK = 2

    def start
      with('status') do
        { type: 'GAME_START', random: rand(1..4) }
      end
    end

    def start_selected(selected)
      if !selected
        start
      else
        with('status') do
          { type: 'GAME_START', selected: selected }
        end
      end
    end

    def start_ok(id)
      with('status') do |s|
        if s['type'] == 'GAME_START'
          selected = s['selected'] || s['random']
          ok = Set.new(s['ok'] || [])
          ok << id
          if ok.size >= REQUIRE_OK
            with('game') do
              new_game(selected)
            end
          else
            { type: 'GAME_START', selected: selected, ok: ok.to_a }
          end
        else
          s
        end
      end
    end

    def start_cancel(id)
      with('status') do |s|
        if s['type'] == 'GAME_START'
          ok = Set.new(s['ok'] || []).delete(id)
          s['ok'] =
            if ok.empty?
              nil
            else
              ok.to_a
            end
        end
        s
      end
    end
  end
end
