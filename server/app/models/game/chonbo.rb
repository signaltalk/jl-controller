class Game
  #
  module Chonbo
    REQUIRE_OK = 2
    CHONBO_BAPPU = [2000, 4000].freeze

    def chonbo(id)
      with('status') { chonbo_result(id) }
    end

    def chonbo_result(id)
      game = current_game
      current_oya = (game['start_seat'] - 1 + game['kyoku'] - 1) % 4 + 1
      diff = {}
      diff['score'] = (1..4).map do |i|
        if i == id
          if id == current_oya
            -(CHONBO_BAPPU.second * 3)
          else
            -(CHONBO_BAPPU.first * 2 + CHONBO_BAPPU.second)
          end
        elsif id == current_oya || i == current_oya
          CHONBO_BAPPU.second
        else
          CHONBO_BAPPU.first
        end
      end
      subject = {
        'target' => id
      }
      require_ok = REQUIRE_OK
      ok = (1..4).reject { |i| i == id }
                 .map { |i| [i.to_s, false] }
                 .to_h
      context = 'chonbo'
      game = current_game
      game['reached'].each_with_index do |r, i|
        game['score'][i] += 1000 if r
      end
      game['kyoutakued'].each_with_index do |r, i|
        game['score'][i] += r * 1000 if r.positive?
      end
      game['reached'] = Array.new(4, false)
      game['kyoutakued'] = Array.new(4, 0)
      with('result') do
        {
          type: 'RESULT',
          context: context,
          subject: subject,
          game: game,
          diff: diff,
          ok: ok,
          require_ok: require_ok
        }
      end
    end
  end
end
