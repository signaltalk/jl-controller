App.control = (args) ->
  args = Object.assign {}, args,
    command: (payload) ->
      @perform 'command', payload
  App.cable.subscriptions.create "ControlChannel", args

