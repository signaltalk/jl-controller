require 'json'

#
class ControlChannel < ApplicationCable::Channel
  def subscribed
    Rails.logger.debug "subscribed: #{params}"
    stream_from 'worker_channel'
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def command(data)
    game = Game.new
    stat =
      case data['type']
      when 'INIT'
        game.init data['id']
      when 'GAME_START'
        game.start
      when 'GAME_RESTART'
        game.restart_game
      when 'GAME_START_SELECTED'
        game.start_selected(data['selected'])
      when 'GAME_START_OK'
        game.start_ok(data['id'])
      when 'GAME_START_CANCEL'
        game.start_cancel(data['id'])
      when 'GAME_REACH'
        game.reach(data['id'])
      when 'GAME_REACH_CANCEL'
        game.reach_cancel(data['id'])
      when 'GAME_KYOUTAKU'
        game.kyoutaku(data['id'])
      when 'GAME_KYOUTAKU_CANCEL'
        game.kyoutaku_cancel(data['id'])
      when 'AGARI'
        game.agari(data['agari'])
      when 'AGARI_CANCEL'
        game.agari_cancel
      when 'AGARI_OK'
        game.agari_ok(data['agari'])
      when 'RYUKYOKU'
        game.ryukyoku
      when 'RYUKYOKU_SELECTED'
        game.ryukyoku_selected(data['id'], data['selected'])
      when 'RYUKYOKU_CANCEL'
        game.current_game
      when 'CHONBO'
        game.chonbo(data['id'])
      when 'RESULT_OK'
        game.result_ok(data['id'], data['ok'])
      when 'GAME_END'
        game.game_end
      when 'GAME_END_CANCEL'
        game.game_end_cancel
      when 'GAME_END_OK'
        game.game_end_ok(data['id'], data['ok'])
      else
        Rails.logger.warn "unknown command type: #{data['type']}"
      end
    publish stat
  end

  def publish(data)
    ActionCable.server.broadcast 'worker_channel', data
  end
end
